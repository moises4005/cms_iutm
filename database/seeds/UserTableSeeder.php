<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create(array('name' => 'iutm', 'email' => 'iutm@iutm.com', 'password' => \Hash::make('iutm2016') ));
    }
}
