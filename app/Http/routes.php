<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('webSite.home.index');
});
Route::get('/cms/login', function () {
    return view('cms.login.index');
});

Route::post('/cms/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/cms/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::group(['middleware' => 'auth'], function () {
	Route::get('/cms/dashboard', function () {
	    return view('cms.dashboard.index');
	});
	Route::get('/cms/events', function () {
	    return view('cms.events.index');
	});
	Route::get('/cms/news', function () {
	    return view('cms.news.index');
	});
	Route::get('/cms/users', function () {
	    return view('cms.users.index');
	});
});
