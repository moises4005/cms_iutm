<!DOCTYPE html>
<html lang="en">

@include('layouts.partials.head')

<body>

    <!-- Page Content -->
    <div class="container">
    	<!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                	SISTEMA CMS IUTM
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">IUTM</a>
                    </li>
                    <li class="active">CMS</li>
                </ol>
                @include('layouts.partials.messages')
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
            	<div class="col-lg-6 col-lg-offset-3">
	            	<div class="panel panel-default">
					    <div class="panel-heading">
					      <h3 class="panel-title">Ingrese al Sistema</h3>
					    </div>
					    <div class="panel-body">
                            {!! Form::open(['url' => '/cms/login', 'role' => 'form', 'method'=>'post']) !!}
                                <h4 class="form-signin-heading text-center">
                                    Ingrese sus Datos
                                </h4>
                                <div class="form-group">
                                    {!! Form::label('email','Correo Electronico',array('for' => 'email')) !!}
                                    {!!Form::text('email_login_cms',null,array('id'=>'email','class'=>'form-control','placeholder'=>'Email address','autofocus'=>'','type'=>'email')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password','Contraseña',array('for' => 'password')) !!}
                                    {!!Form::password('password_login_cms',array('id'=>'password','class'=>'form-control','placeholder'=>'Ingrese su Contraseña','autofocus'=>'')) !!}
                                </div>
                                <div class="form-group">
    								<div class="checkbox">
    									<label>
                                            {!! Form::checkbox('remember',true)!!} Recuerdame
    									</label>
    								</div>
                                </div>
                                 {!! Form::submit('iniciar sesión',array('class'=>'btn btn-lg btn-primary btn-block','type'=>'submit'))!!}
							{!! Form::close() !!}
					    </div>
					</div>
				</div>
            </div>
        </div>
        <!-- Footer -->
        @include('layouts.partials.footer')

    </div>
    <!-- /.container -->

    @include('layouts.partials.script')

</body>

</html>