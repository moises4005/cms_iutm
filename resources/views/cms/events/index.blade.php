@extends('layouts.cms')

@section('page-title')
  Gestión de Eventos
@stop
@section('breadcrumb')
  <li class="active">EVENTOS</li>
@stop
@section('page-container')
	<div class="row">
	    <div class="col-lg-12">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	                <strong>Lista de Eventos</strong>
	            </div>
	            <!-- /.panel-heading -->
	            <div class="panel-body">
	                <div class="dataTable">
	                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                        <thead>
	                            <tr>
	                                <th class="text-center">  ID</th>
	                                <th>Nombre </th>
	                                <th>Apellido</th>
	                                <th>Cedula</th>
	                                <th>Fecha De nacimiento</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	<tr>
	                                <td class="text-center">ID</td>
	                                <td>Nombre </td>
	                                <td>Apellido</td>
	                                <td>Cedula</td>
	                                <td>Fecha De nacimiento</td>
	                            </tr>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	            <!-- /.panel-body -->
	        </div>
	        <!-- /.panel -->
	    </div>
	    <!-- /.col-lg-12 -->
	</div>
@stop
@section('script-after')
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        });
    });
    </script>
@stop