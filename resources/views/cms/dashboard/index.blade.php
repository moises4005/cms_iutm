@extends('layouts.cms')

@section('page-title')
  Bienvenido al Sistema
@stop
@section('breadcrumb')
  <li class="active">DASHBOARD</li>
@stop
@section('page-container')
  <div class="row">
    <div class="col col-md-3">
      <div class="well text-center">
        <i class="fa fa-bullhorn fa-3x"></i>
        <h3>0 
          <small>Eventos</small>
        </h3>
      </div>
    </div>
    <div class="col col-md-3">
      <div class="well text-center">
        <i class="fa  fa-newspaper-o fa-3x"></i>
        <h3>0 
          <small>Noticias</small>
        </h3>
      </div>
    </div>
    <div class="col col-md-3">
      <div class="well text-center">
        <i class="fa  fa-users fa-3x"></i>
        <h3>1
          <small>Usuario</small>
        </h3>
      </div>
    </div>
    <div class="col col-md-3">
      <div class="well text-center">
        <i class="fa  fa-eye fa-3x"></i>
        <h3>0
          <small>Visitas</small>
        </h3>
      </div>
    </div>
  </div>
@stop