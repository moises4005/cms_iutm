@extends('layouts.web-site')
@section('header')
    <!-- Header Carousel -->
    @include('webSite.home.partials.headerCarousel')
@stop
@section('page-container')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Próximos Eventos
            </h1>
        </div>
    </div>
    <!-- /.row -->
    @if (true/*isset($events)*/)
        <!-- Blog Post Row -->
        <div class="row">
            <div class="col-md-6">
                <a href="blog-post.html">
                    <img class="img-responsive img-hover" src="http://placehold.it/600x300" alt="">
                </a>
            </div>
            <div class="col-md-6">
                <h3>
                    <a href="blog-post.html">Evento 1</a>
                </h3>
                <p class='text-success'>
                    <strong>
                        <i class="fa fa-clock-o"></i>
                        Fecha de Evento: August 28, 2013 at 10:00 PM
                    </strong>
                </p>
                <p>Esto es un texto  de prueba que vendrá desde una base de datos, ya que dentro del sistema los evento se cargaran con la información del evento, una o varias fotografiás y ademas estará la fecha en que se realizara el evento, ademas de la fecha de publicación en este sitio web informativo. Cada una de estas secciones representaran un evento determinada y tendrán un botón que dice "Lee más" que lo llevara a una pagina con toda la información extra del evento. es importante aclara que este texto seria un resumen de 545 caracteres máximo...</p>
                <a class="btn btn-primary" href="blog-post.html">Leer Más <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <a href="blog-post.html">
                    <img class="img-responsive img-hover" src="http://placehold.it/600x300" alt="">
                </a>
            </div>
            <div class="col-md-6">
                <h3>
                    <a href="blog-post.html">Evento 2</a>
                </h3>
                <p class='text-success'>
                    <strong>
                        <i class="fa fa-clock-o"></i>
                        Fecha de Evento: August 28, 2013 at 10:00 PM
                    </strong>
                </p>
                <p>Esto es un texto  de prueba que vendrá desde una base de datos, ya que dentro del sistema los evento se cargaran con la información del evento, una o varias fotografiás y ademas estará la fecha en que se realizara el evento, ademas de la fecha de publicación en este sitio web informativo. Cada una de estas secciones representaran un evento determinada y tendrán un botón que dice "Lee más" que lo llevara a una pagina con toda la información extra del evento. es importante aclara que este texto seria un resumen de 545 caracteres máximo...</p>
                <a class="btn btn-primary" href="blog-post.html">Leer Más <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <!-- /.row -->

        <hr>
    @endif

    <hr>

    <!-- Pager -->
    <div class="row">
        <ul class="pager">
            <li class="previous"><a href="#">&larr; Older</a>
            </li>
            <li class="next"><a href="#">Newer &rarr;</a>
            </li>
        </ul>
    </div>
    <!-- /.row -->
@stop
@section('script-after')
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
@stop