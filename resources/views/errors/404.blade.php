<!DOCTYPE html>
<html lang="en">

@include('layouts.partials.head')

<body>

    <!-- Page Content -->
    <div class="container">
    	 <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">404
                    <small>Página no encontrada</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">IUTM</a>
                    </li>
                    <li class="active">404</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="jumbotron">
                    <h1><span class="error-404">404</span>
                    </h1>
                    <p>La página que está buscando no se pudo encontrar.</p>
                </div>
            </div>

        </div>
        <!-- Footer -->
        @include('layouts.partials.footer')

    </div>
    <!-- /.container -->

</body>

</html>