<!DOCTYPE html>
<html lang="en">

@include('layouts.partials.head')

<body>

    <!-- Navigation -->
    @include('layouts.partials.nav')

        @yield("header")

    <!-- Page Content -->
    <div class="container">
    	@yield("page-container","Page Content @section('page-container')")
        <hr>
        <!-- Footer -->
        @include('layouts.partials.footer')

    </div>
    <!-- /.container -->

    @include('layouts.partials.script')
</body>

</html>

