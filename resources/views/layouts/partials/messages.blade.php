@if(Session::has('mensaje_success'))
    <br>
    <!-- Success Alert Content -->
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="fa fa-check-circle"></i> {{Session::get('mensaje_success')}}</h4> Settings <a href="javascript:void(0)" class="alert-link">updated</a>!
    </div>
    <!-- END Success Alert Content -->
@elseif(Session::has('mensaje_info'))
    <br>
    <!-- Info Alert Content -->
    <div class="alert alert-info alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="fa fa-info-circle"></i> {{Session::get('mensaje_info')}}</h4>
    </div>
    <!-- END Info Alert Content -->
@elseif(Session::has('mensaje_warning'))
    <!-- Warning Alert Content -->
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="fa fa-exclamation-circle"></i> Warning</h4>Just to be <a href="javascript:void(0)" class="alert-link">safe</a>!
    </div>
    <!-- END Warning Alert Content -->
@elseif(Session::has('mensaje_dager'))
    <!-- Danger Alert Content -->
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4>
	        <i class="fa fa-times-circle fa-lg"></i>
	        ¡Oh no!
	        {{Session::get('mensaje_dager')}}
	    </h4> 
    </div>
    <!-- END Danger Alert Content -->
@elseif($errors->any())
    <!-- Danger Alert Content -->
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="fa fa-times-circle"></i> Error</h4>
         <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}} <a href="javascript:void(0)" class="alert-link">Destalles</a>!</li>
        @endforeach
        </ul>
    </div>
    <!-- END Danger Alert Content -->
@endif