<!-- jQuery -->
{!! HTML::script('assets/js/jquery.js') !!}

<!-- Bootstrap Core JavaScript -->
{!! HTML::script('assets/js/bootstrap.min.js') !!}

@yield('script-cms')

@yield('script-after')
