<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('cms/dashboard') }}">
          Logo CMS
        </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li>
            <a href="{{ url('/cms/events') }}">
              <i class="fa fa-bullhorn fa-lg"></i>
               Eventos
            </a>
          </li>
          <li>
            <a href="{{ url('cms/news') }}">
              <i class="fa  fa-newspaper-o fa-lg"></i>
               Noticias
            </a>
          </li>
          <li>
            <a href="{{ url('cms/users') }}">
              <i class="fa fa-users fa-lg"></i> 
              Usuarios
            </a>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-user fa-lg"></i> 
              {{ Auth::user()->email }} 
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="#">
                  <i class="fa fa-cogs"></i> 
                  Configuración
                </a>
              </li>
              <li role="separator" class="divider"></li>
              <li class="dropdown-header"></li>
              <li>
                <a href="{{ url('/cms/logout') }}">
                  <i class="fa fa-power-off"></i> 
                  Cerrar Sesión
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
</nav>