<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <img class='center-block img-responsive'src="{{asset('assets/img/head/banner.png')}}" id="banner">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}">
                <img alt="Brand" class='img-responsive'src="{{asset('assets/img/head/logo.png')}}">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-university fa-lg"></i> IUTM
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">ADMINISTRACIÓN</a>
                        </li>
                        <li>
                            <a href="#l">CONTADURÍA</a>
                        </li>
                        <li>
                            <a href="#l">AGROALIMIENTACIÓN</a>
                        </li>
                        <li>
                            <a href="#l">CONSTRUCIÓN CIVIL</a>
                        </li>
                        <li>
                            <a href="#">GEOCIENCIAS</a>
                        </li>
                        <li>
                            <a href="#">INFORMÁTICA</a>
                        </li>
                        <li>
                            <a href="#">MATERIALES INDUSTRIALES</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-graduation-cap fa-lg"></i> 
                        ESTUDIANTES
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">INICIAR SESIÓN</a>
                        </li>
                        <li>
                            <a href="#">REGISTRO</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-search fa-lg"></i>
                         INVESTIGACIÓN
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-map-marker fa-lg"></i>
                         EXTENCIÓN
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-newspaper-o fa-lg"></i>
                         NOTICIAS
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-envelope-o fa-lg"></i>
                         CONTACTO
                    </a>
                </li>
                 <li>
                    <a href="{{url('/cms/login')}}">
                       <i class="fa fa-lock fa-lg"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>