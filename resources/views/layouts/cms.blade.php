<!DOCTYPE html>
<html lang="en">
@section('head-link-cms')
    {!! HTML::style('assets/css/dataTable-1.10.10/dataTables.bootstrap.css')!!}
@stop
@include('layouts.partials.head')

<body>
	<!-- Navigation -->
	@include('layouts.partials.nav-cms')
    <!-- Page Content -->
    <div class="container">
  		<!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <h1 class="page-header">
              @yield('page-title',"Page Title @section('page-title')")
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}">IUTM</a>
                </li>
                <li>CMS</li>
                @yield('breadcrumb',"Breadcrumb @section('breadcrumb')")
            </ol>
            @yield("page-container","Page Content @section('page-container')")
        </div>
        <!-- /.row -->

        <!-- Footer -->
        @include('layouts.partials.footer')
	</div>
    <!-- /.container -->
    @section('script-cms')
        {!! HTML::script('assets/js/dataTable-1.10.10/jquery.dataTables.js') !!}
        {!! HTML::script('assets/js/dataTable-1.10.10/dataTables.bootstrap.js') !!}
    @stop
    @include('layouts.partials.script')

</body>

</html>